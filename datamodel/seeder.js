module.exports = (listService, itemService, userService) => {
    return new Promise(async (resolve, reject) => {

        try {
            await userService.dao.db.query("CREATE TABLE useraccount(id SERIAL PRIMARY KEY, name TEXT NOT NULL, email TEXT NOT NULL, password TEXT NOT NULL)")
            await listService.dao.db.query("CREATE TABLE list(id SERIAL PRIMARY KEY, shop TEXT, date DATE, archived BOOLEAN DEFAULT false, useraccount_id INTEGER REFERENCES useraccount(id))")
            await itemService.dao.db.query("CREATE TABLE item(id SERIAL PRIMARY KEY, label TEXT, quantity NUMERIC,checked BOOLEAN DEFAULT false, list_id INTEGER REFERENCES list(id) ON DELETE CASCADE)")
        }catch(e)  {
            if (e.code === "42P07") { // TABLE ALREADY EXISTS https://www.postgresql.org/docs/8.2/errcodes-appendix.html
                resolve()
            } else {
                reject(e)
            }
            return
        }
         
        userService.insert("User1", "user1@example.com", "azerty")
        userService.insert("User2", "user2@example.com", "azerty")
    })
}