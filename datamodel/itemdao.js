const BaseDAO = require('./basedao')

module.exports = class ItemDAO extends BaseDAO {
    constructor(db) {
        super(db, "item")
    }

    getAll(user) {
        return new Promise((resolve, reject) =>
        this.db.query("SELECT item.id, item.label, item.quantity, item.checked FROM item LEFT JOIN list ON item.list_id = list.id WHERE list.useraccount_id = $1 ORDER BY label", [user.id])
            .then(res => resolve(res.rows))
            .catch(e => reject(e)))
    }

    getById(user, id) {
        return new Promise((resolve, reject) =>
            this.db.query(`SELECT item.id, item.label, item.quantity, item.checked FROM item LEFT JOIN list ON item.list_id = list.id WHERE list.useraccount_id = $1 AND item.id = $2`, [user.id , id])
                .then(res => resolve(res.rows[0]) )
                .catch(e => reject(e)))
    }

    insert(item) {
        return this.db.query("INSERT INTO item(label, quantity, list_id) VALUES ($1,$2,$3)",
            [item.label, item.quantity, item.list_id])
    }

    update(item) {   
        return this.db.query("UPDATE item SET label=$2, quantity=$3, checked=$4 WHERE id=$1",
            [item.id, item.label, item.quantity, list.checked])
    }
}