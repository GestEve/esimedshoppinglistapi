const BaseDAO = require('./basedao')

module.exports = class UserDAO extends BaseDAO {
    constructor(db) {
        super(db, "useraccount")
    }
    insert(user) {
        return this.db.query("INSERT INTO useraccount(name,email,password) VALUES ($1,$2,$3)",
            [user.name, user.email, user.password])
    }
    getByEmail(email) {
        return new Promise((resolve, reject) =>
        this.db.query("SELECT * FROM useraccount WHERE email=$1", [ email ])
            .then(res => resolve(res.rows[0]) )
            .catch(e => reject(e)))
    }

}