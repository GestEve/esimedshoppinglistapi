const BaseDAO = require('./basedao')

module.exports = class ListDAO extends BaseDAO {
    constructor(db) {
        super(db, "list")
    }

    getAll(user) {
        return new Promise((resolve, reject) =>
        this.db.query("SELECT * FROM list WHERE useraccount_id=$1 ORDER BY date", [user.id])
            .then(res => resolve(res.rows))
            .catch(e => reject(e)))
    }

    insert(list) {
        return this.db.query("INSERT INTO list(shop,date,useraccount_id) VALUES ($1,$2,$3)",
            [list.shop, list.date, list.useraccount_id])
    }

    update(list) {   
        return this.db.query("UPDATE list SET shop=$2,date=$3,archived=$4 WHERE id=$1",
            [list.id, list.shop, list.date, list.archived])
    }   

    lastInsert(user) {
        return new Promise((resolve, reject) =>
        this.db.query("SELECT MAX(id) FROM list WHERE useraccount_id=$1", [user.id])
            .then(res => resolve(res.rows[0].max))
            .catch(e => reject(e))) 
    }
}