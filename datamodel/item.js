module.exports = class Item {
    constructor(list_id, label, quantity, checked = false) {
        this.list_id         = list_id 
        this.label           = label
        this.quantity        = quantity
        this.checked         = checked
    }
}