module.exports = class List {
    constructor(shop, date, useraccount_id, archived = false) {
        this.shop            = shop
        this.date            = date
        this.useraccount_id  = useraccount_id
        this.archived        = archived
    }
}