const jwt = require('jsonwebtoken')
const jwtKey = 'exemple_cours_secret_key'
const jwtExpirySeconds = 3600

module.exports = (userService) => {
    return {
        validateJWT(req, res, next) {
            if (req.headers.authorization === undefined) {
                return res.status(401).json({code: 401, error: "unauthorized"}) 
            }
            const token = req.headers.authorization.split(" ")[1];
            jwt.verify(token, jwtKey, {algorithm: "HS256"},  async (err, user) => {
                if (err) {
                    return res.status(401).json({code: 401, error: "unauthorized"}) 
                }
                try {
                    req.user = await userService.dao.getByEmail(user.email)
                    return next()
                } catch(e) {
                    console.log(e)
                    res.status(401).json({code: 401, error: "unauthorized"}) 
                }

            })
        },
        generateJWT(email) {
            return jwt.sign({email}, jwtKey, {
                algorithm: 'HS256',
                expiresIn: jwtExpirySeconds
            })
        }
    }
}
