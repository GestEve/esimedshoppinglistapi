const pg = require('pg');
const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const morgan = require('morgan')

const ListService = require("./services/list")
const ItemService = require("./services/item")
const UserService = require("./services/user")

const app = express()
app.use(bodyParser.urlencoded({ extended: false })) // URLEncoded form data
app.use(bodyParser.json()) // application/json
app.use(cors())
app.use(morgan('dev'))
app.use(cookieParser())

//const connectionString = "postgres://user:password@192.168.56.101/instance"
const connectionString = "postgres://root:root@localhost/esimed_2020_shoppinglist"
const db = new pg.Pool({ connectionString: connectionString })

const listService = new ListService(db)
const itemService = new ItemService(db)
const userService = new UserService(db)
const jwt = require('./jwt')(userService)

require('./api/list')(app, listService, jwt)
require('./api/item')(app, itemService, listService, jwt)
require('./api/auth')(app, userService, jwt)
require('./datamodel/seeder')(listService, itemService, userService)
    .then(app.listen(3000))


