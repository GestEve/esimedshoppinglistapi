const bcrypt = require('bcrypt')
const UserDAO = require('../datamodel/userdao')
const User = require('../datamodel/user')

module.exports = class UserService {
    constructor(db) {
        this.dao = new UserDAO(db)
    }
    insert(name, email, password) {
        return this.dao.insert(new User(name, email, this.hashPassword(password)))
    }
    async validatePassword(email, password) {
        const user = await this.dao.getByEmail(email.trim())
        return this.comparePassword(password, user.password)
    }
    comparePassword(password, hash) {
        return bcrypt.compareSync(password, hash)
    }
    hashPassword(password) {
        return bcrypt.hashSync(password, 10)  //cost 10
    }
}