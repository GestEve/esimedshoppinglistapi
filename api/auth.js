module.exports = (app, userService, jwt) => {
    app.post('/auth', (req, res) => {
        const { email, password } = req.body
        if ((email === undefined) || (password === undefined)) {
            return res.status(400).json({code: 400, error: "bad credentials"}) 
        }
        userService.validatePassword(email, password)
            .then(authenticated => {
                if (!authenticated) {
                    return res.status(401).json({code: 401, error: "unauthorized"}) 
                }
                res.status(200).json({code: 200,'token': jwt.generateJWT(email)})
            })
            .catch(e => {
                console.log(e)
                return res.status(500).json({code: 500, error: "internal Server Error"}) 
            })

    })
}