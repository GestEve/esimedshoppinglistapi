let Item = require('../datamodel/item')

module.exports = (app, item, list, jwt) => {
    app.get("/items", jwt.validateJWT, async (req, res) => {
        const items = await item.dao.getAll(req.user)

        if (items[0] === undefined) {
            return res.status(404).json({code: 404, error: "items not found"})
        }
        res.json(items)
    })

    app.get("/items/:id", jwt.validateJWT, async (req, res) => {
        const id = req.params.id
        
        try {
            const itemTmp = await item.dao.getById(req.user,id)

            if (itemTmp === undefined) {
                return res.status(404).json({code: 404, error: "item not found"})
            }
            
            return res.json(itemTmp)
        } catch (e) { console.log(e); res.status(400).json({code: 400, error: "bad Request"})  }

    })

    app.post("/items", jwt.validateJWT, async (req, res) => {
        const itemTmp = req.body

        if (!item.isValid(itemTmp))  {
            return res.status(400).json({code: 400, error: "bad Request"}) 
        }

        const listTmp = await list.dao.getById(itemTmp.list_id)
        if (listTmp === undefined) {
            return res.status(404).json({code: 404, error: "list not found"})
        }
        if (listTmp.useraccount_id !== req.user.id) {
            return  res.status(404).json({code: 404, error: "list not found"}).end() //404 au lieu de 403 pour dissuader les utilisateurs malveillant
        }
        
        item.dao.insert(itemTmp)
            .then(res.status(203).json({code: 203, message: "item was created"}).end())
            .catch(e => {
                console.log(e)
                res.status(500).json({code: 500, error: "internal server error"}).end()
            })         
    })

    app.put("/items", jwt.validateJWT, async (req, res) => {
        const itemTmp = req.body

        if ((itemTmp.id === undefined) || (itemTmp.id == null) || (!item.isValid(itemTmp))) {
            return  res.status(400).json({code: 400, error: "bad Request"}) 
        }
        const prevItem = await item.dao.getById(req.user, item.id)
        if (prevItem === undefined) {
            return res.status(404).json({code: 404, error: "item not found"})
        }
        if (prevItem.user_id !== req.user.id) {
            return res.status(404).json({code: 404, error: "item not found"})
        }
        item.dao.update(itemTmp)
            .then(res.status(203).json({code: 203, message: "item was updated"}))
            .catch(e => {
                console.log(e)
                res.status(500).json({code: 500, error: "internal server error"}).end()
            })
    })

    app.delete("/items/:id", jwt.validateJWT, async (req, res) => {
        const itemTmp = await item.dao.getById(req.user, req.params.id)
        if (itemTmp === undefined) {
            return res.status(404).json({code: 404, error: "item not found"}).end()
        }

        item.dao.delete(req.params.id)
            .then(res.status(203).json({code: 203, message: "item was deleted"}).end())
            .catch(e => {
                console.log(e)
                res.status(500).json({code: 500, error: "internal server error"}).end()
            })
    })

}
