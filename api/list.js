let List = require('../datamodel/list')
let list = require('../datamodel/list')

module.exports = (app, list, jwt) => {
    app.get("/lists", jwt.validateJWT, async (req, res) => {
        const lists = await list.dao.getAll(req.user)

        if (lists[0] === undefined) {
            return res.status(404).json({code: 404, error: "list not found"})
        }
        res.json(lists)
    })

    app.get("/lists/:id", jwt.validateJWT, async (req, res) => {
        try {
            const listTmp = await list.dao.getById(req.params.id)
            if (listTmp === undefined) {
                return res.status(404).json({code: 404, error: "list not found"}).end()
            }
            if (listTmp.useraccount_id !== req.user.id) {
                return  res.status(404).json({code: 404, error: "list not found"}).end() //404 au lieu de 403 pour dissuader les utilisateurs malveillant
            }
            return res.json(listTmp)
        } catch (e) { console.log(e); res.status(400).json({code: 400, error: "bad Request"}).end()  }
    })

    app.post("/lists", jwt.validateJWT, async (req, res) => {
        const listTmp = req.body

        if (!list.isValid(listTmp))  {
            return res.status(400).json({code: 400, error: "bad Request"}) 
        }
        listTmp.useraccount_id = req.user.id
        
        list.dao.insert(listTmp)
            .then(res.status(203).json({code: 203, id: await list.dao.lastInsert(req.user), message: "list was created"}).end())
            .catch(e => {
                console.log(e)
                res.status(500).json({code: 500, error: "internal server error"}).end()
            })         
    })

    app.put("/lists", jwt.validateJWT, async (req, res) => {
        const listTmp = req.body
        
        if ((listTmp.id === undefined) || (listTmp.id == null) || (!list.isValid(listTmp))) {
            return  res.status(400).json({code: 400, error: "bad Request"}).end() 
        }
        
        const prevList = await list.dao.getById(listTmp.id)
        if (prevList === undefined) {
            return res.status(404).json({code: 404, error: "list not found"}).end()
        }
    
        if (prevList.useraccount_id !== req.user.id) {
            return res.status(404).json({code: 404, error: "list not found"}).end()
        }
        list.dao.update(listTmp)
            .then(res.status(203).json({code: 203, message: "list was updated"}).end())
            .catch(e => {
                console.log(e)
                res.status(500).json({code: 500, error: "internal server error"}).end()
            })
    })

    app.delete("/lists/:id", jwt.validateJWT, async (req, res) => {
        const listTmp = await list.dao.getById(req.params.id)
    
        if (listTmp === undefined) {
            return res.status(404).json({code: 404, error: "list not found"}).end()
        }
        if (listTmp.useraccount_id !== req.user.id) {
            return res.status(404).json({code: 404, error: "list not found"}).end()
        }
        list.dao.delete(req.params.id)
            .then(res.status(203).json({code: 203, message: "list was deleted"}).end())
            .catch(e => {
                console.log(e)
                res.status(500).json({code: 500, error: "internal server error"}).end()
            })
    })

}
